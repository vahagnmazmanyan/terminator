<?php
/**
 * Created by PhpStorm.
 * User: NUM-11
 * Date: 17/01/2019
 * Time: 16:14
 */

namespace NWS\Terminator\interfaces;

/**
 * Interface ColorTypeInterface
 * New color type must implement this
 *
 * @package NWS\Terminator\interfaces
 */
interface ColorTypeInterface
{
    /**
     * Validate $color
     *
     * @param $color
     * @return bool
     */
    public function isValid($color = false);

    /**
     * Get color
     *
     * @param string $type can be 'text' or 'background'
     * @return mixed
     */
    public function get($type = 'text');
}