<?php
/**
 * Created by PhpStorm.
 * User: NUM-11
 * Date: 17/01/2019
 * Time: 15:28
 */

namespace NWS\Terminator;


use NWS\Terminator\helpers\output\AbstractOutWrapper;
use NWS\Terminator\helpers\log\LogHandler;
use NWS\Terminator\helpers\output\OutWrapper;
use NWS\Terminator\helpers\output\StaticBlock;
use NWS\Terminator\helpers\Terminator\TerminatorHelper;
use NWS\Terminator\interfaces\LogHandlerInterface;

/**
 * Class Terminator
 * Main class for working with terminal
 *
 * @method void debug(string $text, bool $log = false)
 * @method void info(string $text, bool $log = false)
 * @method void warn(string $text, bool $log = false)
 * @method void error(string $text, bool $log = false)
 * @method void setAlertSign(string $sign)
 * @method void alert(string $text, string $style = 'debug', bool $log = false)
 * @method StaticBlock createStaticBlock(array $strings, string $blockKey, bool $withHook = false)
 * @method void removeStaticBlock(string | StaticBlock $block)
 * @method void update()
 * @method static array getStyle(string $style)
 * @package NWS\Terminator
 */
class Terminator
{
    use TerminatorHelper;

    /**
     * Terminator version
     *
     * @var string
     */
    protected $version = '1.0 (ALPHA)';

    /**
     * Output wrapper class (Must implement OutWrapperInterface)
     *
     * @var AbstractOutWrapper
     */
    protected static $outputHandler;

    /**
     * Log handler interface (Must implement LogHandlerInterface)
     *
     * @var LogHandlerInterface
     */
    protected static $logHandler;

    /**
     * ANSI sequences support
     *
     * @var bool
     */
    protected $ANSI_SUPPORT = false;

    /**
     * Terminator constructor.
     *
     * @param array $args
     */
    public function __construct(array $args = [])
    {
        $outputHandler = $args['outputHandler'] ?? null;
        $logHandler = $args['logHandler'] ?? null;

        //Set output handler and log handler
        static::$outputHandler = $outputHandler instanceof AbstractOutWrapper ? $outputHandler : new OutWrapper($this);
        static::$logHandler = $logHandler instanceof LogHandlerInterface ? $logHandler : new LogHandler();

        $this->start();
    }

    /**
     * Destruct
     */
    public function __destruct()
    {
        $this->stop();
    }

    /**
     * Redirect calls to output handler
     *
     * @param $name
     * @param $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        if (!method_exists($this, $name)) {
            return call_user_func_array([static::$outputHandler, $name], $arguments);
        }
        return call_user_func_array([$this, $name], $arguments);
    }

    /**
     * Redirect static calls to output handler
     *
     * @param $name
     * @param $arguments
     * @return mixed
     */
    public static function __callStatic($name, $arguments)
    {
        return call_user_func_array([static::$outputHandler, $name], $arguments);
    }
}