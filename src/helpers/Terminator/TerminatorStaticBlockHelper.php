<?php
/**
 * Created by PhpStorm.
 * User: NUM-11
 * Date: 22/01/2019
 * Time: 17:20
 */

namespace NWS\Terminator\helpers\Terminator;


use NWS\Terminator\helpers\output\StaticBlock;

/**
 * Trait TerminatorStaticBlockHelper
 * Static blocks helper properties and methods
 *
 * @package NWS\Terminator\helpers\Terminator
 */
trait TerminatorStaticBlockHelper
{
    /**
     * Static blocks array
     *
     * @var array
     */
    protected $staticBlocks = [];

    /**
     * Static texts lines count
     *
     * @var int
     */
    protected $staticLines = 0;

    /**
     * Check if static blocks registered
     *
     * @return bool
     */
    protected function withStaticBlocks()
    {
        return $this->staticLines > 0;
    }

    /**
     * Get static lines count
     *
     * @return int
     */
    public function getStaticLines()
    {
        return $this->staticLines;
    }

    /**
     * Get static blocks
     *
     * @return array
     */
    protected function getStaticBlocks()
    {
        return $this->staticBlocks;
    }

    /**
     * Add new static block
     *
     * @param StaticBlock $block
     * @param string $key
     */
    public function registerStaticBlock(StaticBlock $block, string $key)
    {
        if ($this->ANSISupports()) {
            $this->staticBlocks[$key] = $block;
            $this->addNewBlockLines($block);
        }
    }

    /**
     * Add new static block lines count
     *
     * @param $block
     */
    public function addNewBlockLines(StaticBlock $block)
    {
        foreach ($block->render() as $text) {
            $this->staticLines += substr_count($text, PHP_EOL);
        }
    }

    /**
     * Remove static block
     *
     * @param $key
     */
    public function removeStaticBlock($key)
    {
        if (isset($this->staticBlocks[$key])) {
            unset($this->staticBlocks[$key]);
        }
    }
}