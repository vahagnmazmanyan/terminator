<?php
/**
 * Created by PhpStorm.
 * User: NUM-11
 * Date: 17/01/2019
 * Time: 18:18
 */

namespace NWS\Terminator\helpers\Terminator;

/**
 * Trait TerminatorHelper
 *
 * @package NWS\Terminator\helpers\Terminator
 */
trait TerminatorHelper
{
    use TerminatorCoreHelper;
    use TerminatorStaticBlockHelper;
}