<?php
/**
 * Created by PhpStorm.
 * User: NUM-11
 * Date: 22/01/2019
 * Time: 17:20
 */

namespace NWS\Terminator\helpers\Terminator;

/**
 * Trait TerminatorCoreHelper
 * Terminal core helper properties and methods
 *
 * @package NWS\Terminator\helpers\Terminator
 */
trait TerminatorCoreHelper
{
    /**
     * Terminal lines count
     *
     * @var int
     */
    protected $lines = 0;

    /**
     * Terminal columns count
     *
     * @var int
     */
    protected $cols = 0;

    /**
     * Is saved cursor
     *
     * @var bool
     */
    protected $savedCursor = false;

    /**
     * Is simple text printed
     *
     * @var bool
     */
    protected $simplePrinted = false;

    /**
     * Change any config
     *
     * @param string $name
     * @param $value
     */
    public function setConfig(string $name, $value)
    {
        if (property_exists($this, $name)) {
            $this->$name = $value;
        }
    }

    /**
     * Get Version
     *
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Get terminal lines count
     *
     * @return int
     */
    public function getLines()
    {
        return $this->lines;
    }

    /**
     * Get terminal columns count
     *
     * @return int
     */
    public function getCols()
    {
        return $this->cols;
    }

    /**
     * Check if Ansi supports
     *
     * @return int
     */
    public function ANSISupports()
    {
        return $this->ANSI_SUPPORT;
    }

    /**
     * Start terminal
     */
    protected function start()
    {
        $this->detectAnsi();
        $this->detectSize();
        if ($this->ANSISupports()) {
            echo chr(27) . "[25l"; // Hide cursor
        }
    }

    /**
     * Stop terminal
     */
    protected function stop()
    {
        if ($this->ANSISupports()) {
            $linesToGoDown = $this->isSimplePrinted() ? ($this->getStaticLines() + ($this->getCols() > 140 && $this->getLines() > 33 ? 5 : 2)) : $this->getStaticLines();
            echo chr(27) . "[25h"; // Show cursor
            echo chr(27) . "[" . $linesToGoDown . "B"; // Move cursor downs
        }
    }

    /**
     * Check if cursor position saved
     *
     * @return bool
     */
    public function isSavedCursor()
    {
        return $this->savedCursor;
    }

    /**
     * Save cursor position
     */
    public function saveCursor()
    {
        $this->savedCursor = true;
    }

    /**
     * Reset cursor saved position
     */
    public function resetCursor()
    {
        $this->savedCursor = false;
    }

    /**
     * Set simplePrinted true
     */
    public function simplePrinted()
    {
        $this->simplePrinted = true;
    }

    /**
     * Check if simple output printed
     *
     * @return bool
     */
    public function isSimplePrinted()
    {
        return $this->simplePrinted;
    }

    /**
     * DETECT ANSI Support
     */
    private function detectAnsi()
    {
        $support = getenv("ANSICON");

        if (!$this->checkTTY()) {
            $this->alert(
                "TERMINATOR version " . $this->getVersion() . PHP_EOL .
                "Your terminal doesn't support ANSI ESC sequences!!!" . PHP_EOL .
                "Some features may not work as expected"
            );
            return;
        }
        //Let terminal use ANSI sequences
        $this->ANSI_SUPPORT = true;
    }

    /**
     * DETECT Terminal sizes
     */
    private function detectSize()
    {
        $sizes = getenv('ANSICON');
        if ($sizes !== false) {
            preg_match_all('/\(([0-9]+)x([0-9]+)\)/i', $sizes, $matches);
            if (!empty($matches) && isset($matches[1][0]) && isset($matches[2][0])) {
                $this->cols = $matches[1][0] ?? 100;
                $this->lines = $matches[2][0] ?? 20;
            }
        }
    }

    /**
     * Check if device is an interactive TTY
     *
     * @return bool
     */
    private function checkTTY()
    {
        if (DIRECTORY_SEPARATOR === '\\') {
            return false !== getenv('ANSICON')
                || 'ON' === getenv('ConEmuANSI')
                || 'xterm' === getenv('TERM');
        }

        return function_exists('posix_isatty') && @posix_isatty(STDOUT);
    }
}