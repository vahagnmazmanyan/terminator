<?php
/**
 * Created by PhpStorm.
 * User: NUM-11
 * Date: 18/01/2019
 * Time: 18:41
 */

namespace NWS\Terminator\helpers\output;


use NWS\Terminator\Terminator;
/**
 * Class StaticBlock
 * Block of texts which is rendering on the same line every time
 *
 * @package NWS\Terminator\helpers\output
 */
class StaticBlock
{
    /**
     * Texts array
     *
     * @var array
     */
    private $texts = [];

    /**
     * Object name
     *
     * @var string
     */
    private $name = '';

    /**
     * Hook on static content update
     *
     * @var callable
     */
    private $hook;

    /**
     * StaticBlock constructor.
     *
     * @param array $texts
     * @param $name
     * @param callable $hook
     */
    public function __construct(array $texts, $name, callable $hook)
    {
        $this->setTexts($texts);
        $this->name = $name;
        $this->onUpdate = $hook;
    }

    /**
     * Set texts in static block
     *
     * @param $texts
     */
    public function setTexts(array $texts)
    {
        foreach ($texts as $key => $text) {
            if (!($text instanceof Text)) {
                continue;
            }
            $this->setText($key, $text);
        }
    }

    /**
     * Set text object
     *
     * @param $key
     * @param $text
     */
    private function setText($key, Text $text)
    {
        $this->texts[$key] = $text;
    }

    /**
     * Get all texts
     *
     * @return array
     */
    public function getTexts()
    {
        return $this->texts;
    }

    /**
     * Add new text
     *
     * @param $key
     * @param $newText
     * @param bool $style
     */
    public function addText($key, $newText, $style = false)
    {
        if (!isset($this->texts[$key])) {
            $newText = $newText instanceof Text ? $newText : new Text($newText, Terminator::getStyle($style !== false ? $style : 'debug'));
            $this->setText($key, $newText);
            $this->updateBlock();
        }
    }

    /**
     * Update existing text
     *
     * @param $key
     * @param $newText
     */
    public function updateText($key, $newText)
    {
        if (isset($this->texts[$key])) {
            $current = $this->texts[$key];
            $newText = $newText instanceof Text ? $newText : new Text($newText, $current->getStyles());
            $this->setText($key, $newText);
            $this->updateBlock();
        }
    }

    /**
     * Remove existing text by key
     *
     * @param $key
     */
    public function deleteText($key)
    {
        if (isset($this->texts[$key])) {
            unset($this->texts[$key]);
            $this->updateBlock();
        }
    }

    /**
     * Update block result
     */
    public function updateBlock()
    {
        return call_user_func($this->onUpdate);
    }

    /**
     * Return rendered texts array
     *
     * @return array
     */
    public function render()
    {
        $texts = array_map(function(Text $text) {
            return $text->render();
        }, $this->texts);

        array_unshift($texts, new Text("##########" . $this->name . "##########", Terminator::getStyle('error')));

        return $texts;
    }

    /**
     * Getter for $name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Getters
     *
     * @param $name
     * @return mixed|null
     */
    public function __get($name)
    {
        return $this->texts[$name] ?? null;
    }
}