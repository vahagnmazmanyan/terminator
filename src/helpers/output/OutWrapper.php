<?php
/**
 * Created by PhpStorm.
 * User: NUM-11
 * Date: 17/01/2019
 * Time: 15:33
 */

namespace NWS\Terminator\helpers\output;


use NWS\Terminator\helpers\StringHelper;

/**
 * Class OutWrapper
 *
 * @package NWS\Terminator\helpers\output
 */
class OutWrapper extends AbstractOutWrapper
{
    /**
     * Debug output
     *
     * @param string $string
     * @param bool $log
     */
    public function debug(string $string, $log = false)
    {
        $text = new Text($string, $this->getStyle('debug'));

        $this->write($text);
    }

    /**
     * Info output
     *
     * @param string $string
     * @param bool $log
     */
    public function info(string $string, $log = false)
    {
        $text = new Text($string, $this->getStyle('info'));

        $this->write($text);
    }

    /**
     * Warning output
     *
     * @param string $string
     * @param bool $log
     */
    public function warn(string $string, $log = false)
    {
        $text = new Text($string, $this->getStyle('warn'));

        $this->write($text);
    }

    /**
     * Error output
     *
     * @param string $string
     * @param bool $log
     */
    public function error(string $string, $log = false)
    {
        $text = new Text($string, $this->getStyle('error'));

        $this->write($text);
    }

    /**
     * Alert output
     *
     * @param string $string
     * @param string $style
     * @param bool $log
     */
    public function alert(string $string, string $style = 'debug', $log = false)
    {
        //Get styles and sign for alert
        $styles = $this->getStyle($style);
        $styles["clearLine"] = true;

        $sign = $this->getAlertSign();

        //Format string
        $strings = explode(PHP_EOL, $string);
        $maxLength = StringHelper::getMaxLength($strings);
        $strings = StringHelper::alignStrings($strings, $maxLength);

        $alertLines = str_repeat($sign, $maxLength + 4);

        //Create lines array to print
        $lines = [];
        $lines[] = new Text($alertLines, $styles);
        foreach ($strings as $printString) {
            $lines[] = new Text("$sign " . $printString . " $sign", $styles);
        }
        $lines[] = new Text($alertLines , $styles);

        //Print result
        $this->write(...$lines);
    }

    /**
     * Create new static block
     *
     * @param array $strings
     * @param $name
     * @param bool $withHook
     * @return StaticBlock
     */
    public function createStaticBlock(array $strings, $name, $withHook = false)
    {
        $texts = [];

        foreach ($strings as $key => $params) {
            $texts[$key] = new Text($params['text'], $this->getStyle($params['style'] ?? 'debug'));
        }
        //Add empty line
        $emptyStyle = $this->getStyle('info');
        $emptyStyle['clearLine'] = true;
        $emptyStyle['withNewLine'] = false;
        $texts[] = new Text("", $emptyStyle);

        $staticBlock = new StaticBlock($texts, $name, function() use($withHook){
            !$withHook ?:$this->update();
        });

        $this->parent->registerStaticBlock($staticBlock, $name);//Register static block
        $this->update();

        return $staticBlock;
    }

    /**
     * Remove registered static block
     *
     * @param $staticBlock
     */
    public function removeStaticBlock($staticBlock)
    {
        if ($staticBlock instanceof StaticBlock) {
            $name = $staticBlock->getName();
        } else {
            $name = $staticBlock;
        }
        $this->parent->removeStaticBlock($name);
        $this->update();
    }
}