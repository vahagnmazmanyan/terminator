<?php
/**
 * Created by PhpStorm.
 * User: NUM-11
 * Date: 17/01/2019
 * Time: 15:31
 */

namespace NWS\Terminator\helpers\output;

/**
 * Class LowLevelOut
 *
 * @package NWS\Terminator\helpers
 */
class LowLevelOut
{
    /**
     * Buffer is started
     *
     * @var bool
     */
    private static $bufferStarted = false;

    /**
     * Just output
     *
     * @param array ...$strings
     */
    public static function out(...$strings)
    {
        static::startBuffer();

        foreach ($strings as &$string) {
            echo $string;
        }
    }

    /**
     * Print buffered string
     */
    public static function printResult()
    {
        $resultString = ob_get_clean();

        echo $resultString; // fwrite(STDOUT, $resultString);
    }

    /**
     * Start buffer if not started
     */
    private static function startBuffer()
    {
        if (!static::$bufferStarted) {
            ob_start();
            static::$bufferStarted = true;
        }
    }
}