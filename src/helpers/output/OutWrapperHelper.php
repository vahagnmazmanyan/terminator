<?php
/**
 * Created by PhpStorm.
 * User: NUM-11
 * Date: 22/01/2019
 * Time: 17:40
 */

namespace NWS\Terminator\helpers\output;

/**
 * Trait OutWrapperHelper
 * Helper methods
 *
 * @package NWS\Terminator\helpers\output
 */
trait OutWrapperHelper
{
    /**
     * Write wrap LowLevelOut
     *
     * @param array ...$strings
     */
    protected function write(...$strings)
    {
        $this->parent->simplePrinted();
        $this->outputSimple($strings);
        $this->outputStatic();

        LowLevelOut::printResult();
    }

    /**
     * Update static blocks, tables etc...
     */
    public function update()
    {
        $this->outputStatic();

        LowLevelOut::printResult();
    }

    /**
     * Output static blocks
     */
    private function outputStatic()
    {
        if ($this->parent->withStaticBlocks() && $this->parent->ANSISupports()) {
            $staticStrings = $this->addStaticSequences($this->parent->getStaticBlocks());
            LowLevelOut::out(...$staticStrings);
        }
    }

    /**
     * Output simple texts
     *
     * @param $strings
     */
    private function outputSimple($strings)
    {
        if (!$this->parent->ANSISupports()) {
            foreach ($strings as &$string) {
                $string = Formatter::cleanSequences($string);
            }
        } else {
            $strings = $this->addSimpleSequences(...$strings);
        }

        LowLevelOut::out(...$strings);
    }

    /**
     * Add ANSI sequences for static block(s)
     *
     * @param array $staticBlocks
     * @return array
     */
    protected function addStaticSequences($staticBlocks)
    {
        $result = [];

        if (empty($staticBlocks)) {
            return $result;
        }

        $newLines = 0;
        if ($this->parent->isSimplePrinted()) {
            if ($this->parent->getCols() > 140 && $this->parent->getLines() > 33) {
                $pref = str_repeat(PHP_EOL . chr(27) . "[K", 5) . PHP_EOL;
            } else {
                $pref = PHP_EOL . chr(27) . "[K" . PHP_EOL;
            }

            $result[] = $pref;
            $newLines += substr_count($pref, PHP_EOL);
        }

        foreach ($staticBlocks as $staticBlock) {
            $texts = $staticBlock->render();
            $lastText = array_last($texts);
            foreach ($texts as $text) {
                if ($text == $lastText) {
                    $text .= chr(27) . "[m";
                }
                $result[] = $text;
                $newLines += substr_count($text, PHP_EOL);
            }
        }

        $result[] = chr(27) . "[" . $newLines . "A"; // Go up by $newLines count

        return $result;
    }

    /**
     * Add ANSI sequences for simple Text(s)
     *
     * @param array ...$strings
     * @return array
     */
    protected function addSimpleSequences(...$strings)
    {
        $result = [];
        foreach ($strings as &$string) {
            $result[] = $string;
        }
        $result[] = chr(27) . "[1L"; // Insert empty line

        return $result;
    }
}