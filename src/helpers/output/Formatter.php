<?php
/**
 * Created by PhpStorm.
 * User: NUM-11
 * Date: 17/01/2019
 * Time: 17:10
 */

namespace NWS\Terminator\helpers\output;

/**
 * Class Formatter
 *
 * @package NWS\Terminator\helpers
 */
class Formatter
{
    /**
     * Is text styled
     *
     * @var bool
     */
    static $styled = false;

    /**
     * Format string with params
     *
     * @param $string
     * @param array $params
     * @return string
     */
    public static function format($string, array $params = [])
    {
        $string = static::addNewLine($string, $params['withNewLine'] ?? false);
        $styles = static::setClear('', $params['clearLine'] ?? false);
        $styles = static::setStyle($styles, $params['style'] ?? 0);
        $styles = static::setColor($styles, $params['color']);
        $styles = static::setBgColor($styles, $params['bgColor']);

        $result = $styles . $string;


        return ($params['finish'] ?? false) === false ? $result : static::finish($result);
    }

    /**
     * Concat new line \n\r
     *
     * @param $string
     * @param $withNewLine
     * @return string
     */
    public static function addNewLine($string, $withNewLine)
    {
        return $withNewLine ? $string . PHP_EOL : $string;
    }

    /**
     * Concat clear line command
     *
     * @param $string
     * @param $clear
     * @return string
     */
    public static function setClear($string, $clear)
    {
        if (!$clear) {
            return $string;
        }
        return chr(27) . "[K" . $string;
    }

    /**
     * Concat color to string
     *
     * @param $string
     * @param null $color
     * @return string
     */
    public static function setColor($string, $color = null)
    {
        if ($color) {
            static::$styled = true;
            $string = $string . $color;
        }

        return $string;
    }

    /**
     * Concat bg color to string
     *
     * @param $string
     * @param null $bgColor
     * @return string
     */
    public static function setBgColor($string, $bgColor = null)
    {
        if ($bgColor) {
            static::$styled = true;
            $string = $string . $bgColor;
        }

        return $string;
    }

    /**
     * Concat style to string
     *
     * @param $string
     * @param null $style
     * @return string
     */
    public static function setStyle($string, $style = null)
    {
        if ($style) {
            static::$styled = true;
            $string = $string . $style;
        }
        return  $string;
    }

    /**
     * Add ANSI finish tag
     *
     * @param $string
     * @return string
     */
    public static function finish($string)
    {
        return static::$styled ? $string . chr(27) . "[m" : $string;
    }

    /**
     * Clean ANSI sequences
     *
     * @param $string
     * @return string
     */
    public static function cleanSequences($string)
    {
        return preg_replace('#\\x1b[[][^A-Za-z]*[A-Za-z]#', '', $string);
    }
}