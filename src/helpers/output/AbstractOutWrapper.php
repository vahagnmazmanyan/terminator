<?php
/**
 * Created by PhpStorm.
 * User: NUM-11
 * Date: 18/01/2019
 * Time: 17:09
 */

namespace NWS\Terminator\helpers\output;


use NWS\Terminator\helpers\output\colors\Color;
use NWS\Terminator\Terminator;

/**
 * Abstract Class AbstractOutWrapper
 * Extend this class to create your own OutWrapper instance
 *
 * @package NWS\Terminator\helpers
 */
abstract class AbstractOutWrapper
{
    use OutWrapperHelper;

    /**
     * Parent terminator
     *
     * @var Terminator
     */
    protected $parent;

    /**
     * Available default colors
     *
     * @var array
     */
    protected $colors = [
        'info' => [
            'color' => 'GREEN',
        ],
        'warn' => [
            'color' => 'YELLOW',
        ],
        'error' => [
            'color' => 'WHITE',
            'bgColor' => 'RED'
        ],
        'debug' => [
            'color' => 'BLACK',
            'bgColor' => 'WHITE'
        ]
    ];

    /**
     * Alert sign
     *
     * @var string
     */
    private $alertSign = "*";

    /**
     * AbstractOutWrapper constructor.
     *
     * @param Terminator $parent
     */
    public function __construct(Terminator $parent)
    {
        $this->parent = $parent;
    }

    /**
     * Setter for alertSign
     *
     * @param $sign
     */
    public function setAlertSign($sign)
    {
        $this->alertSign = $sign;
    }

    /**
     * Getter for alertSign
     *
     * @return string
     */
    public function getAlertSign()
    {
        return $this->alertSign;
    }

    /**
     * Get style
     *
     * @param $style
     * @return mixed
     */
    public function getStyle($style)
    {
        $styleArr = $this->colors[strtolower($style)] ?? $this->colors['debug'];

        array_walk($styleArr, function(&$v, $k) {
            if ($k == 'color') {
                $v = new Color($v);
            } elseif ($k == 'bgColor') {
                $v = new Color($v, 'background');
            }
        });

        return $styleArr;
    }

    /**
     * Debug output
     *
     * @param string $string
     * @param bool $log
     */
    abstract public function debug(string $string, $log = false);

    /**
     * Info output
     *
     * @param string $string
     * @param bool $log
     */
    abstract public function info(string $string, $log = false);

    /**
     * Warning output
     *
     * @param string $string
     * @param bool $log
     */
    abstract public function warn(string $string, $log = false);

    /**
     * Error output
     *
     * @param string $string
     * @param bool $log
     */
    abstract public function error(string $string, $log = false);

    /**
     * Alert output
     *
     * @param string $string
     * @param string $style
     * @param bool $log
     */
    abstract public function alert(string $string, string $style = 'debug', $log = false);
}