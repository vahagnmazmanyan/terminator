<?php
/**
 * Created by PhpStorm.
 * User: NUM-11
 * Date: 17/01/2019
 * Time: 18:20
 */

namespace NWS\Terminator\helpers\output;

/**
 * Class Text
 * Every line in terminal will create instance of this class
 *
 * @package NWS\Terminator\helpers
 */
class Text
{
    /**
     * Available text options
     *
     * @var array
     */
    private $availableKeys = [
        'color',
        'bgColor',
        'style',
        'withNewLine',
        'clearLine'
    ];

    /**
     * Text to print
     *
     * @var string
     */
    private $string = '';

    /**
     * Text color
     *
     * @var string
     */
    private $color;

    /**
     * Background color
     *
     * @var string
     */
    private $bgColor;

    /**
     * Text style
     *
     * @var string
     */
    private $style;

    /**
     * Add PHP_EOL to string
     *
     * @var bool
     */
    private $withNewLine = true;

    /**
     * Clear line before print
     *
     * @var bool
     */
    private $clearLine = true;

    /**
     * Text constructor.
     *
     * @param string $string
     * @param array $options
     */
    public function __construct(string $string, $options = [])
    {
        $this->string = $string;
        if ($options) {
            $this->setOptions($options);
        }
    }

    /**
     * Set text options
     *
     * @param array $options
     */
    public function setOptions(array $options)
    {
        foreach ($this->availableKeys as $key) {
            if (isset($options[$key])) {
                $this->$key = $options[$key];
            }
        }
    }

    /**
     * Get current object styles
     *
     * @return array
     */
    public function getStyles()
    {
        return [
            'color' => $this->color,
            'bgColor' => $this->bgColor,
            'style' => $this->style,
            'withNewLine' => $this->withNewLine,
            'clearLine' => $this->clearLine
        ];
    }

    /**
     * Render formatted string with styles
     *
     * @return string
     */
    public function render()
    {
        $string = $this->string;

        return Formatter::format($string, [
            'withNewLine' => $this->withNewLine,
            'clearLine' => $this->clearLine,
            'color' => $this->color,
            'bgColor' => $this->bgColor,
            'style' => $this->style,
            'finish' => true
        ]);
    }

    /**
     * Return formatted string
     *
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }
}