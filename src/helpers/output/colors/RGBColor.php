<?php
/**
 * Created by PhpStorm.
 * User: NUM-11
 * Date: 17/01/2019
 * Time: 16:11
 */

namespace NWS\Terminator\helpers\output\colors;


use NWS\Terminator\interfaces\ColorTypeInterface;

/**
 * Class RGBColor
 *
 * @package NWS\Terminator\helpers
 */
class RGBColor extends AbstractColorType
{
    /**
     * Validate $color
     *
     * @param $color
     * @return bool
     */
    public function isValid($color = false)
    {
        $this->originalColor = $color ? $color : $this->originalColor;
        $parts = explode(',', $this->originalColor);

        if (count($parts) < 3) {
            return false;
        }

        foreach ($parts as $part){
            if (!is_numeric($part) || $part < 0 || $part > 255) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get color
     *
     * @param string $type can be 'text' or 'background'
     * @return mixed
     */
    public function get($type = 'text')
    {
        $code = $type == 'text' ? 38 : 48;
        list($r,$g,$b) = explode(',', $this->originalColor);
        $this->color = "\033[" . $code . ";2;" . $r . ";" . $g . ";" . $b . "m";

        return $this->color;
    }
}