<?php
/**
 * Created by PhpStorm.
 * User: NUM-11
 * Date: 17/01/2019
 * Time: 15:46
 */

namespace NWS\Terminator\helpers\output\colors;

/**
 * Class Color
 * use this class for colored texts
 *
 * @package NWS\Terminator\helpers
 */
class Color
{
    /**
     * Current color
     *
     * @var string
     */
    private $color;

    /**
     * Color constructor.
     *
     * @param string $color
     * @param string $type
     */
    public function __construct($color = 'DEFAULT', $type = 'color')
    {
        if ($type == 'color') {
            $this->setColor($color);
        } elseif ($type == 'background') {
            $this->setBgColor($color);
        }
    }

    /**
     * Set text color
     *
     * @param $color
     */
    public function setColor($color)
    {
        $this->color = ColorTypeFactory::get($color, 'text');
    }

    /**
     * Set bg color
     *
     * @param $color
     */
    public function setBgColor($color)
    {
        $this->color = ColorTypeFactory::get($color, 'background');
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Get color string
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getColor();
    }
}