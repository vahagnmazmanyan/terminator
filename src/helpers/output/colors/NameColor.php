<?php
/**
 * Created by PhpStorm.
 * User: NUM-11
 * Date: 17/01/2019
 * Time: 16:12
 */

namespace NWS\Terminator\helpers\output\colors;

/**
 * Class NameColor
 *
 * @package NWS\Terminator\helpers\output\colors
 */
class NameColor extends AbstractColorType
{
    /**
     * Available color names
     *
     * @var array
     */
    private static $availableColorNames = [
        "DEFAULT" => "\033[39m",
        "WHITE" => "\033[37m",
        "BLACK" => "\033[30m",
        "RED" => "\033[31m",
        "GREEN" => "\033[32m",
        "YELLOW" => "\033[33m",
        "BLUE" => "\033[34m",
        "PURPLE" => "\033[35m",
        "CYAN" => "\033[36m",
    ];

    /**
     * Available color names
     *
     * @var array
     */
    private static $availableBgColorNames = [
        "DEFAULT" => "\033[49m",
        "WHITE" => "\033[107m",
        "BLACK" => "\033[40m",
        "RED" => "\033[41m",
        "GREEN" => "\033[42m",
        "YELLOW" => "\033[43m",
        "BLUE" => "\033[44m",
        "PURPLE" => "\033[45m",
        "CYAN" => "\033[46m",
    ];

    /**
     * Get available color names
     *
     * @param $type
     * @return array
     */
    public static function getAvailableNames($type = 'text')
    {
        return $type == 'text' ? static::$availableColorNames : static::$availableBgColorNames;
    }

    /**
     * Validate $color
     *
     * @param $color
     * @return bool
     */
    public function isValid($color = false)
    {
        $this->originalColor = $color ? $color : $this->originalColor;
        if (!isset(static::getAvailableNames()[strtoupper($this->originalColor)])) {
            return false;
        }

        return true;
    }

    /**
     * Get color
     *
     * @param string $type can be 'text' or 'background'
     * @return mixed
     */
    public function get($type = 'text')
    {
        return static::getAvailableNames($type)[strtoupper($this->originalColor)];
    }
}