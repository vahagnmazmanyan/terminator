<?php
/**
 * Created by PhpStorm.
 * User: NUM-11
 * Date: 17/01/2019
 * Time: 16:55
 */

namespace NWS\Terminator\helpers\output\colors;


use NWS\Terminator\interfaces\ColorTypeInterface;

/**
 * Class AbstractColorType
 *
 * @package NWS\Terminator\helpers
 */
abstract class AbstractColorType implements ColorTypeInterface
{
    /**
     * Original color to validate and set
     *
     * @var string
     */
    protected $originalColor;

    /**
     * Validated ANSI color string
     *
     * @var string
     */
    protected $color;

    /**
     * AbstractColorType constructor.
     *
     * @param $originalColor
     */
    public function __construct($originalColor = false)
    {
        if ($originalColor) {
            $this->originalColor = $originalColor;
        }
    }
}