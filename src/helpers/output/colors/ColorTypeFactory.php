<?php
/**
 * Created by PhpStorm.
 * User: NUM-11
 * Date: 17/01/2019
 * Time: 16:42
 */

namespace NWS\Terminator\helpers\output\colors;

/**
 * Class ColorTypeFactory
 *
 * @package NWS\Terminator\helpers\output\colors
 */
class ColorTypeFactory
{
    /**
     * Available color types
     *
     * @var array
     */
    private static $availableColorTypes = [
        'RGB' => RGBColor::class,
        'Name' => NameColor::class,
    ];

    private static $defaultColorType = NameColor::class;

    /**
     * Detect color type and get ANSI code
     *
     * @param $color string
     * @param $type string 'text' or 'background'
     * @return string
     */
    public static function get($color, $type)
    {
        $colorTypes = static::getAvailableColorTypes();

        foreach ($colorTypes as $colorType => $colorClass) {
            $object = new $colorClass($color);
            if ($object->isValid()) {
                return $object->get($type);
            }
        }
    }

    /**
     * Get available color types
     *
     * @return array
     */
    public static function getAvailableColorTypes()
    {
        return static::$availableColorTypes;
    }

    /**
     * Add new Color type
     *
     * @param string $name
     * @param string $className
     */
    public static function addColorType(string $name, string $className)
    {
        static::$availableColorTypes[$name] = $className;
    }
}