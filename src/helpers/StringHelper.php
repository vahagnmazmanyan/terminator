<?php
/**
 * Created by PhpStorm.
 * User: NUM-11
 * Date: 18/01/2019
 * Time: 16:50
 */

namespace NWS\Terminator\helpers;

/**
 * Class StringHelper
 * Helper class for working with strings
 *
 * @package NWS\Terminator\helpers
 */
class StringHelper
{
    /**
     * Get max length from array of strings
     *
     * @param array $strings
     * @return int
     */
    public static function getMaxLength(array $strings)
    {
        $maxLength = 0;

        foreach ($strings as &$currentString) {
            $length = mb_strlen($currentString);
            if ($length > $maxLength) {
                $maxLength = $length;
            }
        }

        return $maxLength;
    }

    /**
     * Align strings with max length string
     *
     * @param array $strings
     * @param bool $maxLength
     * @return array
     */
    public static function alignStrings(array $strings, $maxLength = false)
    {
        if ($maxLength === false) {
            $maxLength = static::getMaxLength($strings);
        }

        foreach ($strings as &$string) {
            $currentLength = mb_strlen($string);
            if ($currentLength >= $maxLength) {
                continue;
            }

            $addCount = $maxLength - $currentLength;
            if (($r = $addCount % 2) === 0) {
                $appendCount = $addCount / 2;
                $prependCount = $appendCount;
            } else {
                $appendCount = floor($addCount / 2);
                $prependCount = $appendCount + $r;
            }

            $string = static::append(
                static::prepend(
                    $string,
                    str_repeat(" ", $prependCount)
                ),
                str_repeat(" ", $appendCount)
            );
        }

        return $strings;
    }

    /**
     * Append to string
     *
     * @param $string
     * @param $append
     * @return string
     */
    public static function append($string, $append)
    {
        return $string . $append;
    }

    /**
     * Prepend to string
     *
     * @param $string
     * @param $prepend
     * @return string
     */
    public static function prepend($string, $prepend)
    {
        return $prepend . $string;
    }
}