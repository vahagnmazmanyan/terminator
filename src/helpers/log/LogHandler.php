<?php
/**
 * Created by PhpStorm.
 * User: NUM-11
 * Date: 17/01/2019
 * Time: 15:43
 */

namespace NWS\Terminator\helpers\log;


use NWS\Terminator\interfaces\LogHandlerInterface;

/**
 * Class LogHandler
 * Log handler class must save logs in files
 *
 * @package NWS\Terminator\helpers\log
 */
class LogHandler implements LogHandlerInterface
{
    //TODO: Add logging logic
}