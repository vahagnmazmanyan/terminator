# About

**This package works only in terminals which supports `ANSI ESC sequences`.**

# Usage 

This is very Simple!!!

Just create instance of `NWS\Terminator\Terminator.php` class
```php
<?php
    require('../path/to/autolaod.php');
    
    $terminator = new NWS\Terminator\Terminator();
    
?>
```

That's all now you can use terminator's output methods

### Simple output examples

**Print `information` text.**
```php
<?php
    $terminator->info("Information Log");
?>
```
**Print `warning` text.**
```php
<?php
    $terminator->warn("Warning Log");
?>
```
**Print `error` text.**
```php
<?php
    $terminator->error("Error Log");
?>
```
**Print `debug` text.**
```php
<?php
    $terminator->debug("Debug Log");
?>
```

## For Advanced users

**The best advantage of this package is `StaticBlock` OOP implementation...**

Example, you want to show static text like this.

```markdown
-- count    : {$fullCount}
-- done     : {$doneCount}
-- errors   : {$errorCount}
```

You can create this in a simple way.

```php
<?php
    $terminator = new NWS\Terminator\Terminator();
    
    $fullCount = 100;
    $doneCount = 0;
    $errorCount = 0;
    
    $staticTexts = [
        "count" => [
            "text"  => "-- count     :   " . $fullCount,
            "style" => "info" //Optional
        ],
        "done"  => [
            "text"  => "-- done     :   " . $doneCount,
            "style" => "debug"
        ],
        "error" => [
            "text" => "-- errors    :   " . $errorCount    
        ]   
    ];
    
    $staticBlock = $terminator->createStaticBlock($staticTexts, "STATIC_BLOCK_1");
?>
```

The result you will see in terminal.

```markdown
##########STATIC_BLOCK_1##########               
count : 100                                      
done  : 0                                        
errors: 0                                        
                                                 
```

To test it just run this code.

```php
<?php
        //Create instance
        $terminator = new NWS\Terminator\Terminator();
        
        //Create variables for test
        $fullCount = 100;
        $doneCount = 0;
        $errorCount = 0;
        
        //Create texts for static block
        $staticTexts = [
            "count" => [
                "text"  => "count : " . $fullCount,
                "style" => "info" //Optional
            ],
            "done"  => [
                "text"  => "done  : " . $doneCount,
                "style" => "warn" //Optional
            ],
            "error" => [
                "text" => "errors: " . $errorCount,
                "style" => "error" //Optional
            ]
        ];
        
        //Create static block
        $staticBlock = $terminator->createStaticBlock($staticTexts, "STATIC_BLOCK_1");
        
        //Run test
        $iterator = 0;
        while ($iterator <= $fullCount) {
            $iterator++;
            usleep(100000); //To see in slow mode
            
            if ($iterator % 2 === 0) {
                $errorCount++;
                $staticBlock->updateText("error", "errors: " . $errorCount);
            } else {
                $staticBlock->updateText("done", "done  : " . $doneCount);
                $doneCount++;
            }
            
            $terminator->update();
        }
```

1. **Remove text from static block:** `$staticBlock->removeText($key)`
2. **Add text to static block:** `$staticBlock->addText($key, $text, $style)`
3. **Remove static block:** `$terminator->removeStaticBlock($block)`